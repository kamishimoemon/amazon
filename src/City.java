import java.util.List;
import java.util.ArrayList;

public class City
{
	private int id;
	private List<City> connections;

	City (int id)
	{
		this.id = id;
		this.connections = new ArrayList<City>();
	}

	public void connectTo (City city)
	{
		if (this != city && !this.connections.contains(city))
		{
			this.connections.add(city);
			city.connectTo(this);
		}
	}

	public boolean isConnectedTo (City destination)
	{
		return this.isConnectedDirectlyTo(destination) || this.isConnectedIndirectlyTo(destination, this);
	}

	private boolean isConnectedDirectlyTo (City destination)
	{
		return this.connections.contains(destination);
	}

	private boolean isConnectedIndirectlyTo (City destination, City source)
	{
		for (City intermediary : this.connections)
		{
			if (intermediary != source && intermediary.isConnectedTo(destination, source))
				return true;
		}

		return false;
	}

	private boolean isConnectedTo (City destination, City source)
	{
		return this.isConnectedDirectlyTo(destination) || this.isConnectedIndirectlyTo(destination, source);
	}
}