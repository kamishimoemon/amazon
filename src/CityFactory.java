import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;

public class CityFactory
{
	private int counter;
	private Map<Integer, City> cities;

	private CityFactory ()
	{
		this.counter = 0;
		this.cities = new HashMap<Integer, City>();
	}

	public City createCity ()
	{
		this.counter++;
		this.cities.put(this.counter, new City(this.counter));
		return this.cities.get(this.counter);
	}

	public City getCityById (int id)
	{
		if (!this.cities.containsKey(id))
			throw new IllegalArgumentException();

		return this.cities.get(id);
	}

	public List<City> getCities ()
	{
		return new ArrayList<City>(this.cities.values());
	}

	private static CityFactory instance = new CityFactory();

	public static CityFactory getInstance ()
	{
		return CityFactory.instance;
	}
}