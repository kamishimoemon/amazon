import java.util.List;
import java.util.Queue;
import java.util.LinkedList;
import java.util.Arrays;

public class Amazon
{
	public static int minimumRepairCost (int numTotalAvailableCities, int numTotalAvailableRoads, List<List<Integer>> roadsAvailable, int numRoadsToBeRepaired, List<List<Integer>> costRoadsToBeRepaired)
	{
		int minimumCost = 0;

		CityFactory cityFactory = CityFactory.getInstance();

		for (int i = 1; i <= numTotalAvailableCities; i++)
		{
			cityFactory.createCity();
		}

		for (List<Integer> pair : roadsAvailable)
		{
			City source = cityFactory.getCityById(pair.get(0));
			City destination = cityFactory.getCityById(pair.get(1));
			source.connectTo(destination);
		}

		List<City> cities = cityFactory.getCities();
		costRoadsToBeRepaired.sort((List<Integer> cost1, List<Integer> cost2) -> cost2.get(2) - cost1.get(2));
		Queue<List<Integer>> reparationCosts = new LinkedList<List<Integer>>(costRoadsToBeRepaired);

		while (!Amazon.areAllCitiesAccesible(cities))
		{
			List<Integer> triplet = reparationCosts.poll();

			City source = cityFactory.getCityById(triplet.get(0));
			City destination = cityFactory.getCityById(triplet.get(1));
			source.connectTo(destination);

			minimumCost += triplet.get(2);
		}

		return minimumCost;
	}

	private static boolean areAllCitiesAccesible (List<City> cities)
	{
		if (cities.size() > 1)
		{
			City source = cities.get(0);
			for (int i = 1; i < cities.size(); i++)
			{
				if (!source.isConnectedTo(cities.get(i)))
					return false;
			}
		}

		return true;
	}

	/**
	 @source: https://lh3.googleusercontent.com/-yLOrOUhbLoo/XLOcZOzZK6I/AAAAAAAAVGs/D7mfjMq464kJaZPt8M4LuMK1D0QTBPGXwCK8BGAs/s0/2019-04-14.jpg
	 */
	public static int minimumBuildingTime (int numOfParts, List<Integer> parts)
	{
		int time = 0;
		while (parts.size() > 1)
		{
			parts.sort((p1, p2) -> p1 - p2);
			int p1 = parts.remove(0);
			int p2 = parts.remove(0);
			int p3 = p1 + p2;
			time += p3;
			parts.add(p3);
		}
		return time;
	}

	/**
	 * Example: [4, 6, 8, 12]
	 * 4 6 8 12 -> 0
	 * 8 10 12 -> 10 = 4 + 6
	 * 12 18 -> 28 = 4*2 + 6*2 + 8
	 * 30 -> 58 = 4*3 + 6*3 + 8*2 + 12
	 */
	public static int minimumBuildingTimeV2 (int numOfParts, List<Integer> parts)
	{
		parts.sort((p1, p2) -> p1 - p2);
		int p1 = parts.remove(0);
		int p2 = parts.remove(0);

		int time = 0;
		time += p1 * (numOfParts - 1);
		time += p2 * (numOfParts - 1);
		for (int i = 0; i < parts.size(); i++)
		{
			time += parts.get(i) * (parts.size() - i);
		}
		return time;
	}

	public static void main (String[] args)
	{
		Integer[] parts = new Integer[] {12, 6, 8, 4};
		System.out.println(Amazon.minimumBuildingTime(parts.length, new LinkedList<Integer>(Arrays.asList(parts))));
		System.out.println(Amazon.minimumBuildingTimeV2(parts.length, new LinkedList<Integer>(Arrays.asList(parts))));
	}
}